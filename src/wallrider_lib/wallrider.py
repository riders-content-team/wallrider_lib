import rospy
import math
import time
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32
from wallrider_description.srv import LaserSensor, WallriderControl
from std_srvs.srv import Trigger
import os
import requests
import time

class Robot:
    def __init__(self, robot_name):
        rospy.init_node("wallrider_controller")

        #Topics are disabled
        #self.robot_cmd_vel = rospy.Publisher("/wallrider/cmd_vel", Twist, queue_size=10)

        self._init_robot_control()
        self._init_sensor_services()

        time.sleep(0.5)
        self.ach5_test_flag = False

        self.ach1_flag = False
        self.ach2_flag = False
        self.ach3_flag = False
        self.ach4_flag = False
        self.ach5_flag = False

        self.linear_speed = 0
        self.angular_speed = 0
        self.step_size = 10

        self._init_game_controller()


    def _init_robot_control(self):
        try:
            rospy.wait_for_service("/wallrider_control", 1)

            self.control = rospy.ServiceProxy('/wallrider_control', WallriderControl)
            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "stop"
            rospy.logerr("Service call failed: %s" % (e,))

            return False


    def _init_sensor_services (self):
        try:
            rospy.wait_for_service("/wallrider/get_left_range", 1)
            rospy.wait_for_service("/wallrider/get_right_range", 1)
            rospy.wait_for_service("/wallrider/get_front_range", 1)

            self.get_left_sensor = rospy.ServiceProxy('/wallrider/get_left_range', LaserSensor)
            self.get_right_sensor = rospy.ServiceProxy('/wallrider/get_right_range', LaserSensor)
            self.get_front_sensor = rospy.ServiceProxy('/wallrider/get_front_range', LaserSensor)
            self.get_front_left_sensor = rospy.ServiceProxy('/wallrider/get_front_left_range', LaserSensor)
            self.get_front_right_sensor = rospy.ServiceProxy('/wallrider/get_front_right_range', LaserSensor)

            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "stop"
            rospy.logerr("Service call failed: %s" % (e,))

            return False


    def _init_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 1.0)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)

            resp = self.robot_handle()
            self.status = resp.message

        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "free_roam"

            rospy.logerr("Service call failed: %s" % (e,))

    def _check_game_controller(self):
        resp = self.robot_handle()
        self.status = resp.message


    def left_sensor(self):
        if not self.ach3_flag:
            self.ach3_flag = True
            self.achieve(72)

        resp = self.get_left_sensor()

        if (resp.data == float("inf")):
            return 20
        else:
            return resp.data

    def right_sensor(self):
        if not self.ach3_flag:
            self.ach3_flag = True
            self.achieve(72)

        resp = self.get_right_sensor()

        if (resp.data == float("inf")):
            return 20
        else:
            return resp.data

    def front_sensor(self):
        if not self.ach3_flag:
            self.ach3_flag = True
            self.achieve(72)

        resp = self.get_front_sensor()

        if (resp.data == float("inf")):
            return 20
        else:
            return resp.data

    def front_left_sensor(self):
        if not self.ach3_flag:
            self.ach3_flag = True
            self.achieve(72)
        resp = self.get_front_left_sensor()

        if (resp.data == float("inf")):
            return 20
        else:
            return resp.data

    def front_right_sensor(self):
        if not self.ach3_flag:
            self.ach3_flag = True
            self.achieve(72)
        resp = self.get_front_right_sensor()

        if (resp.data == float("inf")):
            return 20
        else:
            return resp.data


    def move(self, linear_speed):
        if not self.ach1_flag:
            self.ach1_flag = True
            self.achieve(70)

        if not self.ach4_flag:
            self.ach4_flag = True
            if linear_speed == 3:
                self.ach5_test_flag = True
                self.achieve(73)

        if not self.ach5_flag:
            self.ach5_flag = True
            if self.ach5_test_flag and not linear_speed == 3:
                self.ach5_test_flag = False
                self.achieve(74)

        if not self.status == "no_robot":
            if not self.status == "free_roam":
                self._check_game_controller()
            if self.status == "stop":
                return
            self.linear_speed = linear_speed
            if self.control:
                self.control(self.linear_speed, self.angular_speed, self.step_size)
            else:
                return

    def rotate(self, angular_speed):
        if not self.ach2_flag:
            self.ach2_flag = True
            self.achieve(71)

        if not self.status == "no_robot":
            if not self.status == "free_roam":
                self._check_game_controller()

            if self.status == "stop":
                return

            self.angular_speed = angular_speed
            if self.control:
                self.control(self.linear_speed, self.angular_speed, self.step_size)
            else:
                return

    def is_ok(self):
        if not rospy.is_shutdown():
            return True
        else:
            return False

    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })
