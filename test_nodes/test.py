#!/usr/bin/env python
from wallrider_lib.wallrider import Robot
import rospy
import math

robot = Robot("wallrider")

while(robot.is_ok()):
    delta = robot.front_left_sensor() - robot.front_right_sensor()
    robot.move(3)
    robot.rotate(delta)
    
"""
    if robot.front_sensor() > 10:
        robot.move(1)
        robot.rotate(delta)
    else:
        speed = max(robot.front_sensor()*abs(delta), 0.7)

        robot.move(speed)
        robot.rotate(delta)
"""
